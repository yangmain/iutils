package cn.iutils.views.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.iutils.common.service.CrudService;
import cn.iutils.views.dao.ViewsDao;
import cn.iutils.views.entity.Views;

/**
* 查看表 Service层
* @author iutils.cn
* @version 1.0
*/
@Service
@Transactional(readOnly = true)
public class ViewsService extends CrudService<ViewsDao, Views> {

}
