package cn.iutils.views.dao;

import cn.iutils.common.ICrudDao;
import cn.iutils.common.annotation.MyBatisDao;
import cn.iutils.views.entity.Views;

/**
* 查看表 DAO接口
* @author iutils.cn
* @version 1.0
*/
@MyBatisDao
public interface ViewsDao extends ICrudDao<Views> {

}
