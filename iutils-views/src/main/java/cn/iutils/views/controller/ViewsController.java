package cn.iutils.views.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cn.iutils.common.Page;
import cn.iutils.common.utils.JStringUtils;
import cn.iutils.common.BaseController;
import cn.iutils.views.entity.Views;
import cn.iutils.views.service.ViewsService;

/**
* 查看表 控制器
* @author iutils.cn
* @version 1.0
*/
@Controller
@RequestMapping("${adminPath}/views")
public class ViewsController extends BaseController {

    @Autowired
    private ViewsService viewsService;

    @ModelAttribute
    public Views get(@RequestParam(required = false) String id) {
        Views entity = null;
        if (JStringUtils.isNotBlank(id)) {
            entity = viewsService.get(id);
        }
        if (entity == null) {
            entity = new Views();
        }
        return entity;
    }

    @RequiresPermissions("views:view")
    @RequestMapping()
    public String list(Model model, Page<Views> page) {
        model.addAttribute("page", page.setList(viewsService.findPage(page)));
        return "views/list";
    }

    @RequiresPermissions("views:create")
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(Views views,Model model) {
        model.addAttribute("views", views);
        return "views/form";
    }

    @RequiresPermissions("views:create")
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(Views views, RedirectAttributes redirectAttributes) {
        viewsService.save(views);
        addMessage(redirectAttributes,"新增成功");
        return "redirect:"+ adminPath +"/views/update?id="+views.getId();
    }

    @RequiresPermissions("views:update")
    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(Views views, Model model) {
        model.addAttribute("views", views);
        return "views/form";
    }

    @RequiresPermissions("views:update")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(Views views, RedirectAttributes redirectAttributes) {
        viewsService.save(views);
        addMessage(redirectAttributes,"修改成功");
        return "redirect:"+ adminPath +"/views/update?id="+views.getId();
    }

    @RequiresPermissions("views:delete")
    @RequestMapping(value = "/{id}/delete", method = RequestMethod.GET)
    public String delete(@PathVariable("id") String id,int pageNo,int pageSize, RedirectAttributes redirectAttributes) {
        viewsService.delete(id);
        addMessage(redirectAttributes,"删除成功");
        return "redirect:"+ adminPath +"/views?pageNo="+pageNo+"&pageSize="+pageSize;
    }
}
