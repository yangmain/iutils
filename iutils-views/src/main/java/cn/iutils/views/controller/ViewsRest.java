package cn.iutils.views.controller;

import cn.iutils.common.ResultVo;
import cn.iutils.common.BaseController;
import cn.iutils.views.entity.Views;
import cn.iutils.views.service.ViewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 踩插件 接口
 * @author iutils.cn
 * @version 1.0
 */
@RestController
@RequestMapping("${restPath}/views")
public class ViewsRest extends BaseController {

    @Autowired
    private ViewsService viewsService;

    /**
     * 浏览记录 接口
     * @param views
     * @return
     */
    @RequestMapping(value = "/submit",method = RequestMethod.GET)
    public @ResponseBody
    ResultVo submit(Views views){
        ResultVo resultVo = null;
        try {
            Views viewsTemp = viewsService.get(views);
            if(viewsTemp!=null){
                views = viewsTemp;
            }
            views.setHits(views.getHits()+1);
            viewsService.save(views);
            resultVo = new ResultVo(ResultVo.SUCCESS,"1","调用成功",null);
        }catch (Exception e){
            logger.error("浏览记录接口调用失败",e.getMessage());
            resultVo = new ResultVo(ResultVo.FAILURE,"-1","调用失败",null);
        }
        return resultVo;
    }

    /**
     * 获取浏览记录 接口
     * @param views
     * @return
     */
    @RequestMapping(value = "/query",method = RequestMethod.GET)
    public @ResponseBody
    ResultVo query(Views views){
        ResultVo resultVo = null;
        try {
            views = viewsService.get(views);
            resultVo = new ResultVo(ResultVo.SUCCESS,"1","调用成功",views.getHits());
        }catch (Exception e){
            logger.error("获取浏览记录接口调用失败",e.getMessage());
            resultVo = new ResultVo(ResultVo.FAILURE,"-1","调用失败",null);
        }
        return resultVo;
    }

}
